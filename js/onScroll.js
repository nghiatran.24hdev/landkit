const welcome = document.querySelector('.Welcome');
const card = document.querySelector('.About .container .row .column .card');
const text = document.querySelector('.About .container .row .column .Text');
const card1 = document.querySelector('.PRICING .container .row2 .column .card');
const card2 = document.querySelector(
  '.PRICING .container .row2 .column2 .card'
);
const intro = document.querySelector(
  '.About .container .row .column .Introduction'
);
const list = document.querySelectorAll('.About .container .row .column .List');
const dashkit = document.querySelector(
  '.About .container .row .column .dashkit'
);
const scroll = document.querySelector('.scroll');
const feature = document.querySelector('.Feature');
const onscrollpage = document.querySelectorAll('.onscroll');
const onloading = document.querySelectorAll('.normal');
const couterscroll = document.querySelectorAll('.couterscroll');
window.addEventListener('scroll', () => {
  welcome.style = `animation: fadeInBottomscroll 1s both;`;
  feature.style = `animation: fadeInBottomscroll 1s both;`;
  card.style = `animation: fadeInLeftscroll 5s both;`;
  text.style = `animation: fadeInRightscroll 5s both;`;
  intro.style = `animation: fadeInLeftscroll 6s both;`;
  list[0].style = `animation: fadeInLeftscroll 6s both;`;
  list[1].style = `animation: fadeInLeftscroll 6s both;`;
  dashkit.style = `animation: fadeInRightscroll 6s both;`;
  card1.style = `animation: fadeInBottomscroll 6s both;`;
  card2.style = `animation: fadeInBottomscroll 6s both;`;

  srollpos = scroll.getBoundingClientRect().top;

  if (srollpos < window.innerHeight) {
    onloading[0].style = 'display:none;';
    onloading[1].style = 'display:none;';
    onloading[2].style = 'display:none;';
    onscrollpage[0].style = 'display :flex;';
    onscrollpage[1].style = 'display :flex;';
    onscrollpage[2].style = 'display :flex;';
    couterscroll.forEach((counter) => {
      const updateCount = () => {
        //typeof number not string
        const target = +counter.getAttribute('data-target');
        const count = +counter.innerText;
        console.log(count);
        if (count < target) {
          counter.innerText = count + 1;
          setTimeout(updateCount, 20);
        } else {
          counter.innerText = target;
        }
      };

      updateCount();
    });
  }
});
