let tbody = document.querySelector('tbody');
let tr = document.getElementsByTagName('tr');
let select = document.querySelector('select');
let ul = document.querySelector('#pagination');

let ArrTr = [];

//i = 1 , because in i = 0 the tr are displaying thead.
for (let i = 1; i <= tr.length; i++) {
  ArrTr.push(tr[i]);
}
select.onchange = rowCount;
function rowCount(e) {
  let neil = document.querySelectorAll('.list');
  neil.forEach((n) => n.remove());
  let limit = parseInt(e.target.value);

  displayPage(limit);
}

function displayPage(limit) {
  tbody.innerHTML = '';
  for (let i = 0; i < limit; i++) {
    tbody.appendChild(ArrTr[i]);
  }
  const searchInput = document.getElementById('search');
  const rows = document.querySelectorAll('tbody tr');
  console.log(rows);
  searchInput.addEventListener('keyup', function (event) {
    const q = event.target.value.toLowerCase();
    rows.forEach((row) => {
      row.querySelector('td').textContent.toLowerCase().startsWith(q)
        ? (row.style.display = 'table-row')
        : (row.style.display = 'none');
    });
  });
  btngenerator(limit);
}
displayPage(10);

function btngenerator(limit) {
  const nofTr = ArrTr.length;
  if (nofTr <= limit) {
    ul.style = 'display:none;';
  } else {
    let style =
      'display: flex;justify-content: space-between;align-items: center; width: 500px;';
    ul.style = style;

    //tính số page có thể.
    const nofPage = Math.ceil(nofTr / limit);

    for (i = 1; i <= nofPage; i++) {
      let li = document.createElement('li');
      li.className = 'list';
      let a = document.createElement('a');
      a.href = '#';
      a.setAttribute('data-page', i);
      li.appendChild(a);
      a.innerText = i;
      ul.insertBefore(li, ul.querySelector('.next'));

      a.onclick = (e) => {
        let position = e.target.getAttribute('data-page');
        //ex : when click page 1 in pagination the table will display data from tr 0 to ...
        tbody.innerHTML = '';
        position--;
        let start = limit * position;
        let end = start + limit;
        let page = ArrTr.slice(start, end);
        for (let i = 0; i < page.length; i++) {
          let item = page[i];
          tbody.appendChild(item);
          //Search data in table
          const searchInput = document.getElementById('search');
          const rows = document.querySelectorAll('tbody tr');
          console.log(rows);
          searchInput.addEventListener('keyup', function (event) {
            const q = event.target.value.toLowerCase();
            rows.forEach((row) => {
              row.querySelector('td').textContent.toLowerCase().startsWith(q)
                ? (row.style.display = 'table-row')
                : (row.style.display = 'none');
            });
          });
        }
      };
    }
  }
}

function sortTableByColumn(table, column, asc = true) {
  const dirModifier = asc ? 1 : -1;
  const tBody = table.tBodies[0];
  const rows = Array.from(tBody.querySelectorAll('tr'));

  // Sort each row
  const sortedRows = rows.sort((a, b) => {
    const aColText = a
      .querySelector(`td:nth-child(${column + 1})`)
      .textContent.trim();
    const bColText = b
      .querySelector(`td:nth-child(${column + 1})`)
      .textContent.trim();

    return aColText > bColText ? 1 * dirModifier : -1 * dirModifier;
  });

  // Remove all existing TRs from the table , the trs are not sort
  while (tBody.firstChild) {
    console.log(tBody.firstChild);
    tBody.removeChild(tBody.firstChild);
  }

  // Re-add the newly sorted rows
  tBody.append(...sortedRows);

  // add up if sort asc down if sort desc
  table
    .querySelectorAll('th')
    .forEach((th) => th.classList.remove('th-sort-asc', 'th-sort-desc'));
  table
    .querySelector(`th:nth-child(${column + 1})`)
    .classList.toggle('th-sort-asc', asc);
  table
    .querySelector(`th:nth-child(${column + 1})`)
    .classList.toggle('th-sort-desc', !asc);
}

document.querySelectorAll('.styled-table th').forEach((headerCell) => {
  headerCell.addEventListener('click', () => {
    const tableElement = headerCell.parentElement.parentElement.parentElement;
    const headerIndex = Array.prototype.indexOf.call(
      headerCell.parentElement.children,
      headerCell
    );
    const currentIsAscending = headerCell.classList.contains('th-sort-asc');

    sortTableByColumn(tableElement, headerIndex, !currentIsAscending);
  });
});
const searchInput = document.getElementById('search');
const rows = document.querySelectorAll('tbody tr');
console.log(rows);
searchInput.addEventListener('keyup', function (event) {
  const q = event.target.value.toLowerCase();
  rows.forEach((row) => {
    row.querySelector('td').textContent.toLowerCase().startsWith(q)
      ? (row.style.display = 'table-row')
      : (row.style.display = 'none');
  });
});
