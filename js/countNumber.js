const couters = document.querySelectorAll('.counter');

couters.forEach((counter) => {
  const updateCount = () => {
    //typeof number not string
    const target = +counter.getAttribute('data-target');
    const count = +counter.innerText;

    if (count < target) {
      counter.innerText = count + 1;
      setTimeout(updateCount, 10);
    } else {
      counter.innerText = target;
    }
  };

  updateCount();
});
