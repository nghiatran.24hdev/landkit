const mycarousel = document.querySelectorAll('.mycarousel');
const img = document.querySelectorAll(
  '.testimonial .container .row .carousel .mycarousel img'
);
const txt = document.querySelectorAll(
  '.testimonial .container .row .carousel .mycarousel .txt .content'
);
let counter = 1;
const slidefun = (n) => {
  let i;
  for (i = 0; i < mycarousel.length; i++) {
    mycarousel[i].style.display = 'none';
  }

  if (n > mycarousel.length) {
    counter = 1;
  }
  if (n < 1) {
    counter = mycarousel.length;
  }
  mycarousel[counter - 1].style =
    'display: flex; justify-content: center; align-items: center;';
};

slidefun(counter);
const nextSlide = (n) => {
  counter += n;
  console.log(counter);
  if (counter == 2) {
    txt[1].style = 'animation: fadeInRight 1s  both';
    img[1].style = 'animation: fadeVisibility 0.5s  both';
  }
  if (counter == 3) {
    txt[0].style = 'animation: fadeInRight 1s  both';
    img[0].style = 'animation: fadeVisibility 0.5s  both';
  }
  if (counter == 0) {
    txt[1].style = 'animation: fadeInLeft 1s both';
    img[1].style = 'animation: fadeVisibility 0.5s  both';
  }
  if (counter == 1) {
    txt[0].style = 'animation: fadeInLeft 1s both';
    img[0].style = 'animation: fadeVisibility 0.5s  both';
  }
  slidefun(counter);
};
