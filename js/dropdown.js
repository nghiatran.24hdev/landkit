const menu = document.querySelector('.droplistcontainer');
document.querySelector('.open').onclick = function (e) {
  menu.classList.toggle('is--active');
  document.body.style.overflowY = 'hidden';
  e.preventDefault();
};

document.querySelector('.exitmenu').onclick = function (e) {
  menu.classList.remove('is--active');
  document.body.style.overflowY = 'scroll';
  e.preventDefault();
};
