const nameinput = document.getElementById('name');
const emailinput = document.getElementById('email');
const passwordinput = document.getElementById('password');

//check email,pass,name on input field
emailinput.addEventListener('input', () => {
  const emailbox = document.querySelector('.emailbox');
  const emailalert = document.querySelector('.emailalert');

  if (validateEmail(emailinput.value)) {
    emailbox.classList.add('valid');
    emailbox.classList.remove('invalid');
    emailalert.innerHTML = 'Your Email in valid';
  } else {
    emailbox.classList.remove('valid');
    emailbox.classList.add('invalid');
    emailalert.innerHTML = 'Your Email address is error';
  }
});

nameinput.addEventListener('input', () => {
  const namebox = document.querySelector('.namebox');
  const namealert = document.querySelector('.namealert');

  var input = nameinput.value.toString();

  var i = 0;
  var char = '';

  if (input.length < 8) {
    namealert.innerHTML = 'the name must longer than 8';
    namebox.classList.add('invalid');
    namebox.classList.remove('valid');
  } else {
    /*  while (i <= input.length) {
      char = input.charAt(i);
      if (isNaN(char)) {
        namealert.innerHTML = 'the name dont have any number ';
        namebox.classList.add('invalid');
        namebox.classList.remove('valid');
      } else if (char == char.toUpperCase()) {
        namealert.innerHTML = 'the name dont have any uppercase character ';
        namebox.classList.add('invalid');
        namebox.classList.remove('valid');
      } else{*/
    namealert.innerHTML = 'the name in valid ';
    namebox.classList.remove('invalid');
    namebox.classList.add('valid');
  }
  //}
  // }
});

passwordinput.addEventListener('input', () => {
  const passbox = document.querySelector('.passbox');
  const passalert = document.querySelector('.passwordalert');

  var input = passwordinput.value.toString();

  if (input.length < 8) {
    passalert.innerHTML = 'the password must longer than 8';
    passbox.classList.add('invalid');
    passbox.classList.remove('valid');
  } else {
    passalert.innerHTML = 'the password in valid ';
    passbox.classList.remove('invalid');
    passbox.classList.add('valid');
  }
  //}
  // }
});

const btn = document
  .getElementById('btndownload')
  .addEventListener('click', () => {
    const emailbox = document.querySelector('.emailbox');
    const emailalert = document.querySelector('.emailalert');
    const namebox = document.querySelector('.namebox');
    const namealert = document.querySelector('.namealert');
    const passbox = document.querySelector('.passbox');
    const passalert = document.querySelector('.passwordalert');
    var inputname = nameinput.value.toString();
    var inputpass = passwordinput.value.toString();
    if (validateEmail(emailinput.value)) {
      emailbox.classList.add('valid');
      emailbox.classList.remove('invalid');
      emailalert.innerHTML = 'Your Email in valid';
    } else if (inputname.length < 8) {
      namealert.innerHTML = 'the name must longer than 8';
      namebox.classList.add('invalid');
      namebox.classList.remove('valid');
    } else if (inputpass.length < 8) {
      passalert.innerHTML = 'the password must longer than 8';
      passbox.classList.add('invalid');
      passbox.classList.remove('valid');
    } else {
      passalert.innerHTML = 'the password in valid ';
      passbox.classList.remove('invalid');
      passbox.classList.add('valid');
      namealert.innerHTML = 'the name in valid ';
      namebox.classList.remove('invalid');
      namebox.classList.add('valid');
      emailbox.classList.remove('valid');
      emailbox.classList.add('invalid');
      emailalert.innerHTML = 'Your Email address is error';
    }
  });

//check email
const validateEmail = (input) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(input);
};
